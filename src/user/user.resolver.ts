import { Args, Info, Query, Resolver, Mutation, Subscription, } from '@nestjs/graphql';
import { PrismaService } from '../prisma/prisma.service';
import { BatchPayload, User } from '@prisma/client';
import { NotImplementedException } from '@nestjs/common';

@Resolver('User')
export class UserResolver {
  constructor(private readonly prisma: PrismaService) {}

  @Query('user')
  async getUsers(@Args() args, @Info() info): Promise<User[]> {
    return this.prisma.user.findMany(args);
  }

  @Query('user')
  async getUser(@Args() args, @Info() info): Promise<User> {
    return this.prisma.user.findOne(args);
  }

  @Mutation('createUser')
  async createUser(@Args() args, @Info() info): Promise<User> {
    return this.prisma.user.create(args);
  }

  @Mutation('updateUser')
  async updateUser(@Args() args, @Info() info): Promise<User> {
    return this.prisma.user.update(args);
  }

  @Mutation('updateManyUsers')
  async updateManyUsers(@Args() args, @Info() info): Promise<BatchPayload> {
    return this.prisma.user.updateMany(args);
  }

  @Mutation('deleteUser')
  async deleteUser(@Args() args, @Info() info): Promise<User> {
    return this.prisma.user.delete(args);
  }

  @Mutation('deleteManyUsers')
  async deleteManyUsers(@Args() args, @Info() info): Promise<BatchPayload> {
    return this.prisma.user.deleteMany(args);
  }

  @Subscription('user')
  onUserMutation(@Args() args, @Info() info) {
    throw new NotImplementedException();
  }
}
