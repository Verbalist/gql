import { Injectable } from '@nestjs/common';
import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql';
import { schema } from './schema';
import { createContext } from './context';

@Injectable()
export class GraphqlConfigService implements GqlOptionsFactory {
    async createGqlOptions(): Promise<GqlModuleOptions> {
        return {
            debug: true,
            playground: true,
            schema,
            context: createContext()
        };
    }
}
