DROP TABLE IF EXISTS "User";
CREATE TABLE if not exists "public"."User"
(
  id SERIAL PRIMARY KEY  NOT NULL,
  name    VARCHAR(255),
  email   VARCHAR(255) UNIQUE NOT NULL
);

DROP TABLE IF EXISTS "Post" CASCADE;
CREATE TABLE if not exists "public"."Post"
(
  id   SERIAL PRIMARY KEY NOT NULL,
  title     VARCHAR(255)       NOT NULL,
  content   TEXT,
  author_id INTEGER,
  FOREIGN KEY (author_id) REFERENCES "public"."User" (id) on delete cascade
);

DROP TABLE IF EXISTS "Profile";
CREATE TABLE if not exists "public"."Profile"
(
  id SERIAL PRIMARY KEY NOT NULL,
  bio        TEXT,
  user_id    INTEGER            NOT NULL,
  FOREIGN KEY (user_id) REFERENCES "public"."User" (id) on delete cascade
);
