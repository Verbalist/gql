import { mutationType, objectType, queryType } from 'nexus';

export const User = objectType({
    name: 'User',
    definition(t) {
        t.model.id();
        t.model.email();
        t.model.name();
        t.model.post();
        t.model.profile();
    },
});

export const Post = objectType({
    name: 'Post',
    definition(t) {
        t.model.id();
        t.model.title();
        t.model.content();
        t.model.author_id();
    },
});

export const Profile = objectType({
    name: 'Profile',
    definition(t) {
        t.model.id();
        t.model.bio();
        t.model.user_id();
    },
});


export const Query = queryType({
    definition(t) {
        t.crud.user();
        t.crud.users({ ordering: true });
        // t.crud.post();
        // t.crud.posts({ filtering: true });
        // t.crud.bio();
        // t.crud.bios({ ordering: true });
    },
});

export const Mutation = mutationType({
    definition(t) {
        t.crud.createOneUser();
        t.crud.createOnePost();
    },
});


