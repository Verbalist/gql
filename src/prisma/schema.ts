import { makeSchema } from 'nexus';
import * as types from './types';
import { nexusPrismaPlugin } from 'nexus-prisma';
import * as path from 'path';

export const schema = makeSchema({
    types,
    plugins: [nexusPrismaPlugin()],
    outputs: {
        schema: path.join(__dirname, './generated/schema.graphql'),
        typegen: path.join(__dirname, './generated/nexus.d.ts'),
    },
    typegenAutoConfig: {
        contextType: 'Context.Context',
        sources: [
            {
                source: '@prisma/client',
                alias: 'prisma',
            },
            {
                source: require.resolve(path.join(__dirname, './context')),
                alias: 'Context',
            },
        ],
    },

});
