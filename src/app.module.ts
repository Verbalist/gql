import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { GraphqlConfigService } from './prisma/graphql-config.service';

@Module({
    imports: [
        GraphQLModule.forRootAsync({
            useClass: GraphqlConfigService
        }),
        UserModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
