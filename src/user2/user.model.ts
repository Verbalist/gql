import { Field, Int, ObjectType } from 'type-graphql';
import { Post } from './post.model';
import { Profile } from './profile.model';

@ObjectType()
export class User {
    @Field(type => Int)
    id: number;

    @Field({ nullable: true })
    name?: string;

    @Field({ nullable: true })
    email: string;

    @Field(type => [Post])
    posts: Post[];

    @Field(type => [Profile])
    profile: Profile[];
}
