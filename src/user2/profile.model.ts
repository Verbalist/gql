import { Field, Int, ObjectType } from 'type-graphql';
import { User } from './user.model';

@ObjectType()
export class Profile {
    @Field(type => Int)
    id: number;

    @Field({ nullable: true })
    bio?: string;

    @Field(type => User)
    user: User[];
}
