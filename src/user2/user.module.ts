import { Module } from '@nestjs/common';
import { PrismaModule } from './../prisma/prisma.module';
import { UserResolver } from './user.resolver';

@Module({
  providers: [UserResolver],
  imports: [PrismaModule],
})
export class UserModule {}
