import { Args, Info, Mutation, Query, Resolver, Subscription, } from '@nestjs/graphql';
import { PrismaService } from '../prisma/prisma.service';
import { NotImplementedException } from '@nestjs/common';
import { User } from './user.model';
import { Int } from 'type-graphql';
import { UserCreateInput, UserUpdateInput } from '@prisma/client';

@Resolver(of => User)
export class UserResolver {
    constructor(private readonly prisma: PrismaService) {
    }

    @Query(returns => [User])
    async getUsers(@Args() args, @Info() info) {
        return this.prisma.user.findMany(args);
    }

    @Query(returns => User)
    async getUser(@Args({ name: 'id', type: () => Int }) id: number, @Info() info) {
        return this.prisma.user.findOne({ where: { id } });
    }

    @Mutation('createUser')
    async createUser(@Args() args: UserCreateInput, @Info() info) {
        return this.prisma.user.create({ data: args });
    }

    @Mutation('updateUser')
    async updateUser(@Args() args: UserUpdateInput, @Info() info) {
        return this.prisma.user.update({ data: args, where: { id: args.id } });
    }

    // @Mutation('updateManyUsers')
    // async updateManyUsers(@Args() args, @Info() info) {
    //     return this.prisma.user.updateMany(args);
    // }
    //
    // @Mutation('deleteUser')
    // async deleteUser(@Args() args, @Info() info) {
    //     return this.prisma.user.delete(args);
    // }
    //
    // @Mutation('deleteManyUsers')
    // async deleteManyUsers(@Args() args, @Info() info) {
    //     return this.prisma.user.deleteMany(args);
    // }

    @Subscription('user')
    onUserMutation(@Args() args, @Info() info) {
        throw new NotImplementedException();
    }
}
