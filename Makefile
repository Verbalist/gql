init:
	make migrate
	make introspect
	make generate
	npm run seed

migrate:
	psql postgres://app:root@10.1.0.3:5432/app -f src/prisma/schema.sql

introspect:
	npx prisma2 introspect --schema=./src/prisma/schema.prisma

generate:
	npx prisma2 generate --schema=./src/prisma/schema.prisma
	npm run generate:nexus

dev:
	npm run start
